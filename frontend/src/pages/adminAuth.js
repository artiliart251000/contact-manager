import React, {useState} from 'react';
import {Button, Card, Container, Form} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {AdminAPI} from "./API/AdminAPI";

const AdminAuth = () => {
    const navigate = useNavigate();
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")
    const loginAdmin = () => {
        const data = {
            password: password,
            email: email,
        }
        AdminAPI.loginUser(data).then(response => {
            if (response.status === 200) {
                window.localStorage.setItem('token', response.data.token)
                navigate('/adminPage')
            }
        })
    }
    return (
        <Container className="d-flex justify-content-center align-items-center"
                   style={{height: window.innerHeight-54}}>
            <Card style={{width: 600}} className="p-5">
                <h2 className="m-auto">Авторизация Администратора</h2>
                <Form className="d-flex flex-column">
                    <Form.Control
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                        className="mt-3"
                        placeholder="Введите ваш email..."
                    />
                    <Form.Control
                        onChange={(e) => setPassword(e.target.value)}
                        type="password"
                        value={password}
                        className="mt-3"
                        placeholder="Введите ваш пароль..."
                    />
                    <Button className="mt-3 align-self-end" variant={"outline-success"} onClick={() => loginAdmin()}>
                        Войти
                    </Button>
                </Form>
            </Card>
        </Container>
    );
};

export default AdminAuth;