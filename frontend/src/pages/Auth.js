import React from 'react';
import {useNavigate} from "react-router-dom";
import {Button, ButtonGroup, Card, Container} from "react-bootstrap";



const Auth = () => {
    const navigate = useNavigate();
    const adminNavigate = () =>{
        navigate('/adminlogin')
    }
    const managerNavigate = () =>{
        navigate('/managerlogin')
    }
    const techNavigate = () =>{
        navigate('/techlogin')
    }
    return (
        <Container
            style = {{height: window.innerHeight -54}}
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 600}} className="p-5">
                <h2 style={{marginBottom: "30px"}} className="d-flex justify-content-center ">Авторизация</h2>
                <div className="d-flex justify-content-center align-items-center">
                    <ButtonGroup style={{width:500}} aria-label="Basic example" >
                        <Button style={{margin: "1px"}} variant="primary" onClick={() =>adminNavigate()} >Войти как администратор</Button>
                        <Button style={{margin: "1px"}} variant="primary" onClick={() =>managerNavigate()} >Войти как абонент. отдел</Button>
                        <Button style={{margin: "1px"}} variant="primary" onClick={() =>techNavigate()} >Войти как тех. отдел</Button>
                    </ButtonGroup>
                    </div>
            </Card>
        </Container>
    );
};

export default Auth;