import React, {useEffect, useState} from 'react';
import {Button, Container, Form} from "react-bootstrap";
import managerPage from './managerPage.css'
import TableOrderProfile from "./orderTable";
import {useNavigate} from "react-router-dom";
import {ManagerAPI} from "../API/ManagerAPI";


const ManagerPage = () => {
    const [name, setName] = useState("")
    const [surname, setSurname] = useState("")
    const getData = () => {
        ManagerAPI.readyOrders().then((response) =>{
            if(response.status === 200){
                setData(response.data)
            }
        })
    }
    useEffect(()=>{
        getData()
    },[])
    const [data, setData] = useState(null)
    const filter = () =>{
        const data ={
            surname: surname,
            name: name
        }
        if (surname !== "" || name !== ""){
            ManagerAPI.findOrder(data).then((response) => {
                setData(response.data)
            })}
    }
    useEffect(()=>{
        filter()
        if (surname === "" && name === ""){
            getData()
        }
    },[surname, name])
    const navigate = useNavigate();
    const orderNavigate = () =>{
        navigate('/managerOrderCreate')
    }
    return (
        <Container className="d-flex align-items-right" style={
            {display: "flex" ,
            flexDirection: "column"
            }}>
            <div className="buttonWrapper">
                <Button className="mt-3 align-self-end" variant={"outline-success"} onClick={() => orderNavigate()}>
                    Создать бланк-заказ
                </Button>
            </div>
            <Form className="d-flex ">
                <Form.Control
                    onChange={(e) => setSurname(e.target.value)}
                    value={surname}
                    className="mt-3"
                    placeholder="Фамилия..."
                />
                <Form.Control
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    className="mt-3 "
                    style={{marginLeft : "14px"}}
                    placeholder="Имя..."
                />
            </Form>
            <div className="tableWrapper">
            <TableOrderProfile orderData={data} getData={getData}/>
            </div>
        </Container>
    );
}


export default ManagerPage;