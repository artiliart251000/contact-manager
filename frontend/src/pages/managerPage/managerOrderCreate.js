import React, {useState} from 'react';
import {useForm} from 'react-hook-form'
import managerPage from './managerPage.css'
import Form from 'react-bootstrap/Form'
import {Button, Container, FloatingLabel} from "react-bootstrap";
import {ManagerAPI} from "../API/ManagerAPI";


const ManagerOrderCreate = () => {
    const [outsideNumbers, setOutsideNumbers] = useState("")
    const [amountAddedLines, setAmountAddedLines] = useState("")
    const [amountInLines, setAmountInLines] = useState("")
    const [amountInLinesWithNumbers, setAmountInLinesWithNumbers] = useState("")
    const [describeRoutingInCall, setDescribeRoutingInCall] = useState("")
    const [withoutAnswer, setWithoutAnswer] = useState("")
    const [busy, setBusy] = useState("")
    const [notAvailable, setNotAvailable] = useState("")
    const [voiceGreetings, setVoiceGreetings] = useState("")
    const [prefixCity, setPrefixCity] = useState("")
    const [technology, setTechnology] = useState("")
    const [amount, setAmount] = useState("")
    const [whoSetupPhones, setWhoSetupPhones] = useState("")
    const [choose, setChoose] = useState("")
    const [IP, setIP] = useState("")
    const [personalAccount, setPersonalAccount] = useState("")
    const [personalAccountAccess, setPersonalAccountAccess] = useState("")
    const [callRecording, setCallRecording] = useState("")
    const [blacklistSize, setBlacklistSize] = useState("")
    const [otherOperatorNumbersAmount, setOtherOperatorNumbersAmount] = useState("")
    const [statisticsModule, setStatisticsModule] = useState("")
    const [additionalCustomerRequests, setAdditionalCustomerRequests] = useState("")
    const [surname, setSurname] = useState("")
    const [name, setName] = useState("")
    const [patronymic, setPatronymic] = useState("")

    const createOrder = () => {
        const objectForm = {
            outsideNumbers: outsideNumbers,
            amountAddedLines: amountAddedLines,
            amountInLines: amountInLines,
            amountInLinesWithNumbers: amountInLinesWithNumbers,
            describeRoutingInCall: describeRoutingInCall,
            redirectRequired:{
                withoutAnswer: withoutAnswer,
                busy: busy,
                notAvailable: notAvailable
            },
            voiceGreetings: voiceGreetings,
            prefixCity: prefixCity?prefixCity:null,
            phonesInUse:{
                technology: technology,
                amount: amount,
            },
            whoSetupPhones: whoSetupPhones,
            otherInternet:{
                choose: choose,
                IP: IP?IP:null,
            },
            personalAccount: personalAccount,
            personalAccountAccess: personalAccountAccess?personalAccountAccess:null,
            callRecording: callRecording,
            blacklistSize: blacklistSize,
            otherOperatorNumbersAmount: otherOperatorNumbersAmount,
            statisticsModule: statisticsModule,
            additionalCustomerRequests: additionalCustomerRequests?additionalCustomerRequests:null,
            surname: surname,
            name: name,
            patronymic: patronymic?patronymic:null,
        }
        ManagerAPI.orderCreate(objectForm).then((response) =>{
            if(response.status === 200){
                console.log(response)
            }
        })
    }

    return (
        <div>
            <Container className="mt-3 ">
            <Form.Label htmlFor="inputPassword5">Внешние телефонные номера</Form.Label>
            <Form.Control
                onChange={(e)=>setOutsideNumbers(e.target.value)}
                value={outsideNumbers}
                id="inputPassword5"
            />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество дополнительных телефонных линий, соответствующих каждому внешнему номеру (если телефон многоканальный)</Form.Label>
                <Form.Control
                    onChange={(e)=>setAmountAddedLines(e.target.value)}
                    value={amountAddedLines}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество внутренних линий</Form.Label>
                <Form.Control
                    onChange={(e)=>setAmountInLines(e.target.value)}
                    value={amountInLines}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество внутренних телефонных линий, с указанием номеров и соответствующих им имен (например: 11-Иванов или 100 Директор)</Form.Label>
                <Form.Control
                    onChange={(e)=>setAmountInLinesWithNumbers(e.target.value)}
                    value={amountInLinesWithNumbers}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Описать своими словами или изобразить схематично маршрутизацию входящего вызова: Какие телефоны звонят при поступлении вызова, продолжительность вызова
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setDescribeRoutingInCall(e.target.value)}
                    value={describeRoutingInCall}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. Без ответа
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setWithoutAnswer(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. При занятости
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setBusy(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. При недоступности
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setNotAvailable(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Наличие голосового приветствия при поступлении вызова. Если «Да», то Заказчику необходимо записать голосовое приветствие самостоятельно и предоставить Исполнителю в формате wav, 8кГц
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setVoiceGreetings(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать префикс, через который будет осуществляться выход в город (обычно «9», либо без него)
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setPrefixCity(e.target.value)}
                    value={prefixCity}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать телефонные аппараты, которые будут использоваться. Аналоговые или IP
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setTechnology(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Аналоговый">Аналоговый</option>
                    <option value="IP" >IP</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать телефонные аппараты, которые будут использоваться. Количество
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setAmount(e.target.value)}
                    value={amount}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Кто будет заниматься настройкой телефонных аппаратов
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setWhoSetupPhones(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Заказчик">Заказчик</option>
                    <option value="Исполнитель" >Исолнитель</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Регистрация внутренних телефонных номеров на сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес»)
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setChoose(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Дополнительный «Внешний IP адрес» (Оставьте пустым если нет)
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setIP(e.target.value)}
                    value={IP}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Личный кабинет для просмотра статистики
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setPersonalAccount(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Доступ в личный кабинет с сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес») (При отказе оставить пустыым)
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setPersonalAccountAccess(e.target.value)}
                    value={personalAccountAccess}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Запись разговоров(архив - 2 недели)
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setCallRecording(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Черный список, шт
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setBlacklistSize(e.target.value)}
                    value={blacklistSize}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Подключение городских телефонных номеров др.оператора, шт
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setOtherOperatorNumbersAmount(e.target.value)}
                    value={otherOperatorNumbersAmount}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Модуль статистики
                </Form.Label>
                <Form.Select aria-label="Default select example"
                             onChange={(e)=> setStatisticsModule(e.target.value)}>
                    <option value="error">Выбор варианта</option>
                    <option value="Да">Да</option>
                    <option value="Нет" >Нет</option>
                </Form.Select>
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Дополнительные требования и пожелания Заказчика
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setAdditionalCustomerRequests(e.target.value)}
                    value={additionalCustomerRequests}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Фамилия заказчика
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setSurname(e.target.value)}
                    value={surname}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Имя заказчика
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setName(e.target.value)}
                    value={name}
                    id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Отчество заказчика (Оставить пустым если отсутствует)
                </Form.Label>
                <Form.Control
                    onChange={(e)=>setPatronymic(e.target.value)}
                    value={patronymic}
                    id="inputPassword5"
                    className="mb-5"
                />
                <div className="buttonWrapper">
                    <Button className= "mb-5" variant={"outline-success"} onClick={() => createOrder()}>
                    Создать
                    </Button>
                    </div>
            </Container>
        </div>
    );
};

export default ManagerOrderCreate;