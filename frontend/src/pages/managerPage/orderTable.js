import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CardMedia from "@mui/material/CardMedia";
import styled from "styled-components/macro";
import {useNavigate} from "react-router-dom";


export default function TableOrderProfile(props) {
    const {
        orderData,
    } = props
    const navigate = useNavigate();


    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 650}} aria-label="simple table">
                <TableHead>
                    <TableRow align='center'>
                        <TableCell align='center'>Фамилия</TableCell>
                        <TableCell align='center'>Имя</TableCell>
                        <TableCell align='center'>Статус заявки</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {orderData?.map((row) => (
                        <TableRow
                            onClick={()=>{
                                navigate(`/managerOrderCheck/${row._id}`)
                            }}
                            key={row._id}
                            sx={{'&:last-child td, &:last-child th': {border: 0}, cursor: "pointer"}}
                        >
                            <TableCell align='center'>{row.surname}</TableCell>
                            <TableCell align='center'>{row.name}</TableCell>
                            <TableCell align='center'>{row.orderStatus}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}