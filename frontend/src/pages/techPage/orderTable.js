import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CardMedia from "@mui/material/CardMedia";
import styled from "styled-components/macro";
import {useNavigate, useParams} from "react-router-dom";
import {TechAPI} from "../API/TechAPI";
import {Button} from "@mui/material";


export default function OrderTable(props) {
    const {
        orderData,
    } = props
    const navigate = useNavigate();

    const params = useParams()
    const orderId = params.id
    const changeOrderStatus = (orderId) => {
        TechAPI.changeOrderStatus(orderId).then((response) =>{
            if(response.status === 200){
                props.getData()
            }
        })
    }


    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 650}} aria-label="simple table">
                <TableHead>
                    <TableRow align='center'>
                        <TableCell align='center'>Фамилия</TableCell>
                        <TableCell align='center'>Имя</TableCell>
                        <TableCell align='center'>Статус заявки</TableCell>
                        <TableCell align='center'>Выполнение</TableCell>
                        <TableCell align='center'>Просмотреть заявку</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {orderData?.map((row) => (
                        <TableRow
                            key={row._id}
                            sx={{'&:last-child td, &:last-child th': {border: 0}, }}
                        >
                            <TableCell align='center'>{row.surname}</TableCell>
                            <TableCell align='center'>{row.name}</TableCell>
                            <TableCell align='center'>{row.orderStatus}</TableCell>
                            <TableCell align='center' onClick={() => changeOrderStatus(row._id)}><Button variant="contained" sx={{cursor: "pointer"}}>Выполнить заказ</Button></TableCell>
                            <TableCell align='center' onClick={() => {
                                navigate(`/techOrderCheck/${row._id}`)
                            }}><Button variant="contained" sx={{cursor: "pointer"}}>Просмотреть заказ</Button></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}