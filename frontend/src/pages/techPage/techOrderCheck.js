import React, {useState, useEffect} from 'react';
import {useForm} from 'react-hook-form'
import Form from 'react-bootstrap/Form'
import {Button, Container, FloatingLabel} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {TechAPI} from "../API/TechAPI";


const TechOrderCheck = () => {
    const params = useParams()
    const orderId = params.id
    const getData = () => {
        TechAPI.orderById(orderId).then((response) =>{
            if(response.status === 200){
                setData(response.data)
            }
        })
    }
    const [data, setData] = useState("")
    useEffect(()=>{
        getData()
    },[orderId])

    useEffect(()=>{
        if(data){
            setOutsideNumbers(data.outsideNumbers)
            setOutsideNumbers(data.outsideNumbers)
            setAmountAddedLines(data.amountAddedLines)
            setAmountInLines(data.amountInLines)
            setAmountInLinesWithNumbers(data.amountInLinesWithNumbers)
            setDescribeRoutingInCall(data.describeRoutingInCall)
            setWithoutAnswer(data.redirectRequired.withoutAnswer)
            setBusy(data.redirectRequired.busy)
            setNotAvailable(data.redirectRequired.notAvailable)
            setVoiceGreetings(data.voiceGreetings)
            setPrefixCity(data.prefixCity)
            setTechnology(data.phonesInUse.technology)
            setAmount(data.phonesInUse.amount)
            setWhoSetupPhones(data.whoSetupPhones)
            setChoose(data.otherInternet.choose)
            setIP(data.otherInternet.IP)
            setPersonalAccount(data.personalAccount)
            setPersonalAccountAccess(data.personalAccountAccess)
            setCallRecording(data.callRecording)
            setBlacklistSize(data.blacklistSize)
            setOtherOperatorNumbersAmount(data.otherOperatorNumbersAmount)
            setStatisticsModule(data.statisticsModule)
            setAdditionalCustomerRequests(data.additionalCustomerRequests)
            setSurname(data.surname)
            setName(data.name)
            setPatronymic(data.patronymic)
        }
    },[data])

    const [outsideNumbers, setOutsideNumbers] = useState(data?data.outsideNumbers:"")
    const [amountAddedLines, setAmountAddedLines] = useState("")
    const [amountInLines, setAmountInLines] = useState("")
    const [amountInLinesWithNumbers, setAmountInLinesWithNumbers] = useState("")
    const [describeRoutingInCall, setDescribeRoutingInCall] = useState("")
    const [withoutAnswer, setWithoutAnswer] = useState("")
    const [busy, setBusy] = useState("")
    const [notAvailable, setNotAvailable] = useState("")
    const [voiceGreetings, setVoiceGreetings] = useState("")
    const [prefixCity, setPrefixCity] = useState("")
    const [technology, setTechnology] = useState("")
    const [amount, setAmount] = useState("")
    const [whoSetupPhones, setWhoSetupPhones] = useState("")
    const [choose, setChoose] = useState("")
    const [IP, setIP] = useState("")
    const [personalAccount, setPersonalAccount] = useState("")
    const [personalAccountAccess, setPersonalAccountAccess] = useState("")
    const [callRecording, setCallRecording] = useState("")
    const [blacklistSize, setBlacklistSize] = useState("")
    const [otherOperatorNumbersAmount, setOtherOperatorNumbersAmount] = useState("")
    const [statisticsModule, setStatisticsModule] = useState("")
    const [additionalCustomerRequests, setAdditionalCustomerRequests] = useState("")
    const [surname, setSurname] = useState("")
    const [name, setName] = useState("")
    const [patronymic, setPatronymic] = useState("")

    return (
        <div>
            <Container className="mt-3 ">
                <Form.Label htmlFor="inputPassword5">Внешние телефонные номера</Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setOutsideNumbers(e.target.value)}
                              value={outsideNumbers}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество дополнительных телефонных линий, соответствующих каждому внешнему номеру (если телефон многоканальный)</Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setAmountAddedLines(e.target.value)}
                              value={amountAddedLines}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество внутренних линий</Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setAmountInLines(e.target.value)}
                              value={amountInLines}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Количество внутренних телефонных линий, с указанием номеров и соответствующих им имен (например: 11-Иванов или 100 Директор)</Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setAmountInLinesWithNumbers(e.target.value)}
                              value={amountInLinesWithNumbers}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">Описать своими словами или изобразить схематично маршрутизацию входящего вызова: Какие телефоны звонят при поступлении вызова, продолжительность вызова
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setDescribeRoutingInCall(e.target.value)}
                              value={describeRoutingInCall}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. Без ответа
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setWithoutAnswer(e.target.value)}
                              value={withoutAnswer}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. При занятости
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setBusy(e.target.value)}
                              value={busy}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Необходима ли переадресация внутренних телефонов между собой. При недоступности
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setNotAvailable(e.target.value)}
                              value={notAvailable}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Наличие голосового приветствия при поступлении вызова. Если «Да», то Заказчику необходимо записать голосовое приветствие самостоятельно и предоставить Исполнителю в формате wav, 8кГц
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setVoiceGreetings(e.target.value)}
                              value={voiceGreetings}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать префикс, через который будет осуществляться выход в город (обычно «9», либо без него)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setPrefixCity(e.target.value)}
                              value={prefixCity}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать телефонные аппараты, которые будут использоваться. Аналоговые или IP
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setTechnology(e.target.value)}
                              value={technology}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Указать телефонные аппараты, которые будут использоваться. Количество
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setAmount(e.target.value)}
                              value={amount}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Кто будет заниматься настройкой телефонных аппаратов
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setWhoSetupPhones(e.target.value)}
                              value={whoSetupPhones}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Регистрация внутренних телефонных номеров на сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес»)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setChoose(e.target.value)}
                              value={choose}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Дополнительный «Внешний IP адрес» (Оставьте пустым если нет)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setIP(e.target.value)}
                              value={IP}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Личный кабинет для просмотра статистики
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setPersonalAccount(e.target.value)}
                              value={personalAccount}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Доступ в личный кабинет с сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес») (При отказе оставить пустыым)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setPersonalAccountAccess(e.target.value)}
                              value={personalAccountAccess}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Запись разговоров(архив - 2 недели)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setCallRecording(e.target.value)}
                              value={callRecording}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Черный список, шт
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setBlacklistSize(e.target.value)}
                              value={blacklistSize}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Подключение городских телефонных номеров др.оператора, шт
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setOtherOperatorNumbersAmount(e.target.value)}
                              value={otherOperatorNumbersAmount}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Модуль статистики
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setStatisticsModule(e.target.value)}
                              value={statisticsModule}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Дополнительные требования и пожелания Заказчика
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setAdditionalCustomerRequests(e.target.value)}
                              value={additionalCustomerRequests}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Фамилия заказчика
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setSurname(e.target.value)}
                              value={surname}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Имя заказчика
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setName(e.target.value)}
                              value={name}
                              id="inputPassword5"
                />
                <Form.Label htmlFor="inputPassword5" className="mt-2">
                    Отчество заказчика (Оставить пустым если отсутствует)
                </Form.Label>
                <Form.Control disabled={true}
                              onChange={(e)=>setPatronymic(e.target.value)}
                              value={patronymic}
                              id="inputPassword5"
                              className="mb-5"
                />
            </Container>
        </div>
    );
};

export default TechOrderCheck;