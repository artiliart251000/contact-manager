import React, {useEffect, useState} from 'react';
import {Button, Card, Container, Form} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {TechAPI} from "../API/TechAPI";
import OrderTable from "./orderTable";

const TechPage = () => {
    const [name, setName] = useState("")
    const [surname, setSurname] = useState("")
    const getData = () => {
        TechAPI.createdOrders().then((response) =>{
            if(response.status === 200){
                setData(response.data)
            }
        })
    }
    useEffect(()=>{
        getData()
    },[])
    const [data, setData] = useState(null)
    const filter = () =>{
        const data ={
            surname: surname,
            name: name
        }
        if (surname !== "" || name !== ""){
        TechAPI.findOrder(data).then((response) => {
            setData(response.data)
        })}
    }
    useEffect(()=>{
        filter()
        if (surname === "" && name === ""){
            getData()
        }
    },[surname, name])
    return (
        <Container className="d-flex align-items-right" style={
            {display: "flex" ,
                flexDirection: "column"
            }}>
            <Form className="d-flex ">
                <Form.Control
                    onChange={(e) => setSurname(e.target.value)}
                    value={surname}
                    className="mt-3"
                    placeholder="Фамилия..."
                />
                <Form.Control
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    className="mt-3 "
                    style={{marginLeft : "14px"}}
                    placeholder="Имя..."
                />
            </Form>
            <div className="tableWrapper">
                <OrderTable orderData={data} getData={getData}/>
            </div>
        </Container>
    );
};

export default TechPage;