import axios from "axios";

const token = localStorage.getItem('token');

const instance = axios.create({
    baseURL: 'http://localhost:5050/api/',
    headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${token}`
    }
});

export const TechAPI = {
    loginUser(data) {
        return (
            instance.post('tech/login', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    createdOrders() {
        return (
            instance.get('tech/orders', )
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    changeOrderStatus(id) {
        return (
            instance.get(`tech/changeOrderState/${id}`,)
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    orderById(id) {
        return (
            instance.get(`tech/orderById/${id}`, )
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    findOrder(data){
        return (
            instance.post('tech/findOrder', {surname: data.surname, name: data.name})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
}