import axios from "axios";

const token = localStorage.getItem('token');

const instance = axios.create({
    baseURL: 'http://localhost:5050/api/',
    headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${token}`
    }
});

export const ManagerAPI = {
    loginUser(data) {
        return (
            instance.post('manager/login', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    readyOrders() {
        return (
            instance.get('manager/readyOrders', )
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    orderById(id) {
        return (
            instance.get(`manager/orderById/${id}`, )
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    orderCreate(data) {
        return (
            instance.post('manager/createOrder', {...data})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    findOrder(data){
        return (
            instance.post('manager/findOrder', {surname: data.surname, name: data.name})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },

}
