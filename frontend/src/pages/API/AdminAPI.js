import axios from "axios";

const token = localStorage.getItem('token');

const instance = axios.create({
    baseURL: 'http://localhost:5050/api/',
    headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${token}`
    }
});

export const AdminAPI = {
    loginUser(data) {
        return (
            instance.post('admin/login', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    createAdmin(data){
        return (
            instance.post('admin/createAdmin', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    createManager(data){
        return (
            instance.post('admin/createManager', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
    createTech(data){
        return (
            instance.post('admin/createTech', {email: data.email, password: data.password})
                .then(function (response) {
                    return response
                })
                .catch(err => {
                    if (err.response) {
                        return err.response
                    }
                })
        )
    },
}