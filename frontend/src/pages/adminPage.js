import React, {useState} from 'react';
import {BrowserRouter} from "react-router-dom";
import {Button, Card, Container, Form} from "react-bootstrap";
import {AdminAPI} from "./API/AdminAPI";


const AdminPage = () => {
    const [department, setDepartment] = useState()
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")
    const createUser = () => {
        const data = {
            password: password,
            email: email,
        }
        if(department === "admin"){
        AdminAPI.createAdmin(data).then(response => {
            if (response.status === 200) {
                alert("Администратор успешно зарегистрирован")
            }
            if (response.status === 400){
                alert("Администратор с данной почтой уже создан")
            }
            if (response.status === 403){
                alert("Вы не авторизованы попробуйте обновить страницу")
            }
        })}
        if(department === "manager"){
            AdminAPI.createManager(data).then(response => {
                if (response.status === 200) {
                    alert("Работник абонент. отдела успешно зарегистрирован")
                }
                if (response.status === 400){
                    alert("Работник абонент. отдела с данной почтой уже создан")
                }
                if (response.status === 403){
                    alert("Вы не авторизованы попробуйте обновить страницу")
                }
            })
        }
        if(department === "tech"){
            AdminAPI.createTech(data).then(response => {
                if (response.status === 200) {
                    alert("Работник тех. отдела успешно зарегистрирован")
                }
                if (response.status === 400){
                    alert("Работник тех. отдела с данной почтой уже создан")
                }
                if (response.status === 403){
                    alert("Вы не авторизованы попробуйте обновить страницу")
                }
            })
        }
    }
    return (
            <Container className="d-flex justify-content-center align-items-center"
                       style={{height: window.innerHeight-54}}>
                <Card style={{width: 600}} className="p-5">
                    <h2 className="m-auto">Регистрация работника</h2>
                    <Form.Select aria-label="Default select example" className="mt-3" onChange={(e)=> setDepartment(e.target.value)}>
                        <option value="error">Выбери отдел</option>
                        <option value="admin" >Администратор</option>
                        <option value="manager">Абонент. отдел</option>
                        <option value="tech">Тех. отдел</option>
                    </Form.Select>
                    <Form className="d-flex flex-column">
                        <Form.Control
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            className="mt-3"
                            placeholder="Введите email..."
                        />
                        <Form.Control
                            onChange={(e) => setPassword(e.target.value)}
                            type="password"
                            value={password}
                            className="mt-3"
                            placeholder="Введите пароль..."
                        />
                        <Button className="mt-3 align-self-end" variant={"outline-success"} onClick={() => createUser()}>
                            Зарегистрировать
                        </Button>
                    </Form>
                </Card>
            </Container>
    );
};

export default AdminPage;