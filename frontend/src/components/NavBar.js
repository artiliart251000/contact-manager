import React from 'react';
import {Container, Navbar, Button} from "react-bootstrap";
import logo from './logo.svg'
import {useNavigate} from "react-router-dom";

const NavBar = () => {
    const navigate = useNavigate();
    const exit = () =>{
        window.localStorage.removeItem("token");
        navigate('/./')
    }
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="/./">
                    <img
                        alt=""
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        style={{marginRight: "5px"}}
                    />
                    Client-manager project
                </Navbar.Brand>
                <Button variant="light" onClick={()=> exit()}> Выйти </Button>
            </Container>
        </Navbar>
    );
};

export default NavBar;