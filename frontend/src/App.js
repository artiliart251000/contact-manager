import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Auth from "./pages/Auth";
import AdminAuth from "./pages/adminAuth";
import Navbar from "./components/NavBar";
import ManagerAuth from "./pages/ManagerAuth";
import TechAuth from "./pages/techAuth";
import AdminPage from "./pages/adminPage";
import ManagerPage from "./pages/managerPage/managerPage";
import ManagerOrderCreate from "./pages/managerPage/managerOrderCreate";
import ManagerOrderCheck from "./pages/managerPage/managerOrderCheck";
import TechPage from "./pages/techPage/techPage";
import TechOrderCheck from "./pages/techPage/techOrderCheck";

const App = () => {
    return (
        <BrowserRouter>
            <Navbar />
            <Routes>
                <Route path={'//'} element={<Auth/>}/>
                <Route path={'/adminLogin/'} element={<AdminAuth/>}/>
                <Route path={'/managerLogin/'} element={<ManagerAuth/>}/>
                <Route path={'/techLogin/'} element={<TechAuth/>}/>
                <Route path={'/adminPage'} element={<AdminPage/>}/>
                <Route path={'/managerPage'} element={<ManagerPage/>}/>
                <Route path={'/managerOrderCreate'} element={<ManagerOrderCreate/>}/>
                <Route path={"/managerOrderCheck/:id"} element={<ManagerOrderCheck/>}/>
                <Route path={"/techPage"} element={<TechPage/>}/>
                <Route path={"/techOrderCheck/:id"} element={<TechOrderCheck/>}/>
            </Routes>
        </BrowserRouter>
    );
};

export default App;
