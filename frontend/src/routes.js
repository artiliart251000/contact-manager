import adminPage from "./pages/adminPage";
import {ADMIN_ROUTE, AUTH_ROUTE, MANAGER_ROUTE, TECH_ROUTE} from "./utils/consts";
import managerPage from "./pages/managerPage/managerPage";
import auth from "./pages/Auth";
import techPage from "./pages/techPage/techPage";

export const authRoutes = [
    {
        path: ADMIN_ROUTE,
        Component: adminPage
    },
    {
        path: MANAGER_ROUTE,
        Component: managerPage
    },
    {
        path: TECH_ROUTE,
        Component: techPage
    }
]

export const publicRoutes = [
    {
        path: AUTH_ROUTE,
        Component: auth
    },
]