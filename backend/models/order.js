const mongoose = require("mongoose")
const Schema = mongoose.Schema

const orderSchema = new Schema({
    //Внешние телефонные номера
    outsideNumbers: {
        type: String,
        required: true,
    },
    //Количество дополнительных телефонных линий, соответствующих каждому внешнему номеру (если телефон многоканальный)
    amountAddedLines: {
        type: String,
        required: true,
    },
    //Количество внутренних линий
    amountInLines: {
        type: String,
        required: true
    },
    //Количество внутренних телефонных линий, с указанием номеров и соответствующих им имен (например: 11-Иванов или 100 Директор)
    amountInLinesWithNumbers: {
        type: String,
        required: true
    },
    //Описать своими словами или изобразить схематично маршрутизацию входящего вызова: Какие телефоны звонят при поступлении вызова, продолжительность вызова
    describeRoutingInCall: {
        type: String,
        required: true
    },
    //Необходима ли переадресация внутренних телефонов между собой
    redirectRequired: {
        //Без ответа
        withoutAnswer: {
            type: String,
            required: true
        },
        //При занятости
        busy: {
            type: String,
            required: true
        },
        //При недоступности
        notAvailable: {
            type: String,
            required: true
        }
    },
    //Наличие голосового приветствия при поступлении вызова. Если «Да», то Заказчику необходимо записать голосовое приветствие самостоятельно и предоставить Исполнителю в формате wav, 8кГц
    voiceGreetings: {
        type: String,
        required: true
    },
    //Указать префикс, через который будет осуществляться выход в город (обычно «9», либо без него)
    prefixCity: {
        type: String,
        default: null,
        required: true
    },
    //Указать телефонные аппараты, которые будут использоваться.
    phonesInUse: {
        //Аналоговые или IP
        technology: {
            type: String,
            required: true
        },
        //Количество
        amount: {
            type: String,
            required: true
        }
    },
    //Кто будет заниматься настройкой телефонных аппаратов
    whoSetupPhones: {
        type: String,
        required: true
    },
    //Регистрация внутренних телефонных номеров на сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес»)
    otherInternet: {
        choose:{
            type: String,
        },
        IP: {
            type: String,
            default: null
        }
    },
    //Личный кабинет для просмотра статистики
    personalAccount: {
        type: String,
        required: true
    },
    //Доступ в личный кабинет с сети другого провайдера Интернета (необходим дополнительный «Внешний IP адрес»)
    personalAccountAccess: {
        type: String,
        default: null
    },
    //Запись разговоров(архив - 2 недели)
    callRecording: {
        type: String,
        required: true
    },
    //Черный список, шт
    blacklistSize: {
        type: String,
        required: true
    },
    //Подключение городских телефонных номеров др.оператора, шт
    otherOperatorNumbersAmount: {
        type: String,
        required: true
    },
    //Модуль статистики
    statisticsModule: {
        type: String,
        required: true
    },
    //Дополнительные требования и пожелания Заказчика
    additionalCustomerRequests: {
        type: String,
        default: null
    },
    //статус заявки
    orderStatus: {
        type: String,
        default: "Создано"
    },
    //Фамилия
    surname:{
        type: String,
        required: true
    },
    //Имя
    name:{
        type: String,
        required: true
    },
    //Отчество
    patronymic:{
        type: String,
    }
})

module.exports = mongoose.model("order", orderSchema)