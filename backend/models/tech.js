const mongoose = require("mongoose")
const Schema = mongoose.Schema

const techSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        default: "TECH"
    }
})

module.exports = mongoose.model("tech", techSchema)