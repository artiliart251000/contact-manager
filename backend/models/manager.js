const mongoose = require("mongoose")
const Schema = mongoose.Schema

const managerSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        default: "MANAGER"
    }
})

module.exports = mongoose.model("manager", managerSchema)