const Router = require('express')
const router = new Router
const adminController = require('../controllers/adminController')
const roleMiddleware = require('../middlewares/roleController')

router.post("/createManager", roleMiddleware(['ADMIN']), adminController.createManager)
router.post("/createTech", roleMiddleware(['ADMIN']), adminController.createTech)
router.post("/createAdmin", roleMiddleware(['ADMIN']), adminController.createAdmin)
router.post("/login", adminController.login)

module.exports = router