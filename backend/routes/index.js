const Router = require('express')
const router = new Router
const adminRouter = require("./adminRouter")
const managerRouter = require("./managerRouter")
const techRouter = require("./techRouter")

router.use('/manager', managerRouter)
router.use('/tech', techRouter)
router.use('/admin', adminRouter)

module.exports = router