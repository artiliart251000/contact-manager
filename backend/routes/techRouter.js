const Router = require('express')
const router = new Router
const techController = require('../controllers/techController')
const roleMiddleware = require('../middlewares/roleController')

router.post('/login', techController.login)
router.post("/findOrder", roleMiddleware(['TECH']), techController.findOrder)
router.get("/orderById/:id", roleMiddleware(['TECH']), techController.orderById)
router.get("/changeOrderState/:id", roleMiddleware(['TECH']), techController.changeOrderState)
router.get("/orders", roleMiddleware(['TECH']), techController.allOrders)

module.exports = router