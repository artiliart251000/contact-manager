const Router = require('express')
const router = new Router
const managerController = require('../controllers/managerController')
const roleMiddleware = require('../middlewares/roleController')

router.post('/login', managerController.login)
router.post('/createOrder', roleMiddleware(['MANAGER']), managerController.createOrder)
router.get('/readyOrders', roleMiddleware(['MANAGER']), managerController.readyOrders)
router.get('/orderById/:id', roleMiddleware(['MANAGER']), managerController.orderById)
router.post("/findOrder", roleMiddleware(['MANAGER']), managerController.findOrder)

module.exports = router