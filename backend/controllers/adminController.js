const Admin = require("../models/admin");
const Manager = require("../models/manager")
const Tech = require("../models/tech")
const bcrypt = require("bcryptjs");
const {secret} = require("../config")
const jwt = require("jsonwebtoken")

const generateAdminToken = (id, role, email) => {
    const payload = {
        id,
        role,
        email,
    }
    return jwt.sign(payload, secret, {expiresIn: "31d"})
}

class adminController {
    async createManager(req, res) {
        try {
            const {email, password} = req.body
            const candidate = await Manager.findOne({email})
            if (candidate) {
                return res.status(400).json({message: "Пользователь с такой почтой уже сущетсвует"})
            }
            const hashPassword = bcrypt.hashSync(password, 7)
            const manager = new Manager({email, password: hashPassword})
            manager.save()
            res.status(200).json({message: "Пользователь успешно зарегистрирован"})
        } catch (e) {
            res.status(500).json(e)
        }
    }

    async createTech(req, res) {
        try {
            const {email, password} = req.body
            const candidate = await Tech.findOne({email})
            if (candidate) {
                return res.status(400).json({message: "Пользователь с такой почтой уже сущетсвует"})
            }
            const hashPassword = bcrypt.hashSync(password, 7)
            const tech = new Tech({email, password: hashPassword})
            tech.save()
            res.status(200).json({message: "Пользователь успешно зарегистрирован"})
        } catch (e) {
            res.status(500).json(e)
        }
    }
    async createAdmin(req, res) {
        try {
            const {email, password} = req.body
            const candidate = await Admin.findOne({email})
            if (candidate) {
                return res.status(400).json({message: "Пользователь с такой почтой уже сущетсвует"})
            }
            const hashPassword = bcrypt.hashSync(password, 7)
            const admin = new Admin({email, password: hashPassword})
            admin.save()
            res.status(200).json({message: "Пользователь успешно зарегистрирован"})
        } catch (e) {
            res.status(500).json(e)
        }
    }

    async login(req, res) {
        try {
            const {username, password} = req.body
            const admin = await Admin.findOne({username})
            if (!admin) {
                return res.status(400).json({message: "Администратор не найден"})
            }
            const validPassword = bcrypt.compareSync(password, admin.password)
            if (!validPassword) {
                return res.status(400).json({message: "Неверный пароль"})
            }
            const token = generateAdminToken(admin._id, admin.role, admin.email)
            return res.json({token})
        } catch (e) {
            res.status(500).json(e)
        }
    }
}

module.exports = new adminController()