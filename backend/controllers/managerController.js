const Manager = require("../models/manager")
const Order = require("../models/order")
const bcrypt = require("bcryptjs");
const {secret} = require("../config")
const jwt = require("jsonwebtoken")

const generateManagerToken = (id, role, email) => {
    const payload = {
        id,
        role,
        email,
    }
    return jwt.sign(payload, secret, {expiresIn: "31d"})
}

class managerController {
    async login(req, res) {
        try {
            const {email, password} = req.body
            const manager = await Manager.findOne({email})
            if (!manager) {
                return res.status(400).json({message: "Менеджер не найден"})
            }
            const validPassword = bcrypt.compareSync(password, manager.password)
            if (!validPassword) {
                return res.status(400).json({message: "Неверный пароль"})
            }
            const token = generateManagerToken(manager._id, manager.role, manager.email)
            return res.json({token})
        } catch (e) {
            res.status(500).json(e)
        }
    }
    async createOrder(req, res){
        try{
            const order = req.body
            const orderSave = new Order(order)
            orderSave.save()
            res.status(200).json(orderSave)
        }catch (e) {
            res.status(500).json(e)
        }
    }
    async readyOrders(req, res){
        try{
            const orders = await Order.find({orderStatus: "Выполнено"})
            res.status(200).json(orders)
        }catch (e) {
            res.status(500).json(e)
        }
    }
    async orderById(req, res){
        try{
            const orders = await Order.findById(req.params.id)
            res.status(200).json(orders)
        }catch(e){
            res.status(500).json(e)
        }
    }
    async findOrder(req,res){
        try {
            const {surname, name} = req.body
            const order = await Order.find({orderStatus: "Выполнено", surname: {$regex: new RegExp('^' + surname, 'i')}, name :{$regex: new RegExp('^' + name, 'i')}})
            res.status(200).json(order)
        }catch (e) {
            res.status(500).json(e)
        }
    }
}

module.exports = new managerController()
