const Tech = require('../models/tech')
const bcrypt = require("bcryptjs");
const {secret} = require("../config")
const jwt = require("jsonwebtoken")
const Order = require("../models/order");

const generateTechToken = (id, role, email) => {
    const payload = {
        id,
        role,
        email,
    }
    return jwt.sign(payload, secret, {expiresIn: "31d"})
}

class techController {
    async login(req, res) {
        try {
            const {email, password} = req.body
            const tech = await Tech.findOne({email})
            if (!tech) {
                return res.status(400).json({message: "Техник не найден"})
            }
            const validPassword = bcrypt.compareSync(password, tech.password)
            if (!validPassword) {
                return res.status(400).json({message: "Неверный пароль"})
            }
            const token = generateTechToken(tech._id, tech.role, tech.email)
            return res.json({token})
        } catch (e) {
            res.status(500).json(e)
        }
    }
    async findOrder(req,res){
        try {
            const {surname, name} = req.body
            const order = await Order.find({orderStatus: "Создано", surname: {$regex: new RegExp('^' + surname, 'i')}, name :{$regex: new RegExp('^' + name, 'i')}})
            res.status(200).json(order)
        }catch (e) {
            res.status(500).json(e)
        }
    }
    async allOrders(req,res){
        try{
            const orders = await Order.find({orderStatus: "Создано"})
            res.status(200).json(orders)
        }catch (e) {
            res.status(500).json(e)
        }
    }
    async changeOrderState(req,res){
        try{
            const order = await Order.findById(req.params.id)
            order.orderStatus = "Выполнено"
            order.save()
            res.status(200).json(order)
        }catch (e) {
            res.status(500).json(e)
        }
    }
    async orderById(req, res){
        try{
            const orders = await Order.findById(req.params.id)
            res.status(200).json(orders)
        }catch(e){
            res.status(500).json(e)
        }
    }
}

module.exports = new techController()