const express = require("express");
const dotenv = require("dotenv");
const cors = require('cors')
const mongoose = require("mongoose");
const router = require('./routes')

const app = express();
app.use(express.json())
app.use('/api', router)
dotenv.config();
app.use(cors())
const PORT = process.env.PORT || 3000

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(console.log("Connected to MongoDB"))
    .catch((err) => console.log(err))

app.listen(PORT, () => {
    console.log(`App is running on port ${PORT}`)
})